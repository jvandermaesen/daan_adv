<!DOCTYPE html>
<html>
<head>
    <title>Hakuna Matata Lodge</title>
    <meta charset="UTF-8"/>
</head>
<body>
<article>
    <h4>Vertel je ervaringen</h4>

    <p><img src="figuren/kampvuur.jpg" alt="kampvuur"/>Informeer onze nieuwe gasten!</p>
</article>
<h3>Gastenboek</h3>
<?php
try {
    $db = new PDO('mysql:host=localhost; dbname=gasten; charset=UTF8', 'root', '');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $db->query('SELECT * FROM gastenboek');

    $count = 1;
    $maxcount = $stmt->rowcount();
    foreach ($stmt as $row):
        ?>
        <p>
            <strong><?= $row['naam'] ?></strong><br/>
            <?= $row['email'] ?><br/>
            <?= $row['datum'] ?><br/><br/>
            <?= $row['bericht'] ?><br/></p>
        <?php if ($count < $maxcount): ?>
            <hr>
        <?php endif;
        $count++; ?>

    <?php
    endforeach;
    //echo "connected";
    //echo $stmt->rowcount();
} catch (Exception $ex) {
    echo "<p>FOUT: " . $ex->getMessage() . "<p>";
}
?>
</body>
</html>